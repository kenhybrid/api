// imports
import fastify from "fastify";
import db from "./helpers/db";
const app = fastify({ logger: true });
// middlewears
app.register(require("fastify-cors"), {
  origin: "*",
});
// custom  packages
app.register(db);
// routes
app.get("/", async (req, res) => {
  res.send({ message: "Welcome to my API" });
});
app.register(
  async (openRoutes) => openRoutes.register(require("./routes/products.js")),
  { prefix: "/api/product" }
);
app.register(
  async (openRoutes) => openRoutes.register(require("./routes/category.js")),
  { prefix: "/api/category" }
);
app.register(
  async (openRoutes) => openRoutes.register(require("./routes/brands.js")),
  { prefix: "/api/brand" }
);
// run the server
const start = async () => {
  try {
    await app.ready();
    await app.listen("8000", "127.0.0.1");
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
start();

export default app;
