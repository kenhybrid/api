// imports
import {
  CreationValidation,
  SearchValidation,
} from "../validations/productValidation.js";
import Product from "../models/Product.js";
import Brand from "../models/Brand.js";
import Category from "../models/Category.js";
import { Conflict, NotFound } from "http-errors";
import { GenerateSKU } from "../helpers/randomizer.js";
// product creation
const ProductCreation = async (req, res) => {
  try {
    const data = await CreationValidation.validateAsync(req.body);
    const { name, brand, category, price, stock, description, color, size } =
      data;

    // check if brand or category exists
    if (brand) {
      const brandExists = await Brand.findOne({ _id: brand });
      if (!brandExists) throw NotFound("brand not found");
    }
    if (category) {
      const categoryExists = await Category.findOne({ _id: category });
      if (!categoryExists) throw NotFound("category not found");
    }
    const sku = await GenerateSKU();
    // product creation
    const product = await new Product({
      name: name,
      brand: brand,
      category: category,
      price: price,
      stock: stock,
      color: color,
      size: size,
      sku: sku,
      description: description,
    });
    const doc = await product.save();

    // updating brand relationship
    if (brand) {
      await Brand.updateOne(
        { _id: doc.brand },
        { $push: { products: doc._id } }
      );
    }

    // updating category relationship
    if (category) {
      await Category.updateOne(
        { _id: doc.category },
        { $push: { products: doc._id } }
      );
    }

    res.send({ message: "success", doc: doc });
  } catch (error) {
    //   errors creation and catching
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// products pagination and listing
const ProductsAll = async (req, res) => {
  try {
    // pagination function
    const limit = 12;
    const page = parseInt(req.query.page) >= 0 ? parseInt(req.query.page) : 1;
    const pagination = {};
    const endIndex = page * limit;
    const startIndex = (page - 1) * limit;
    const count = await Product.countDocuments().exec();
    let allPages;
    // getting all pages
    if ((await Product.countDocuments().exec()) % limit > 0) {
      allPages = parseInt((await Product.countDocuments().exec()) / limit + 1);
    } else {
      allPages = (await Product.countDocuments().exec()) / limit;
    }
    // assigning pagination config
    pagination.pages = allPages;
    pagination.limit = limit;
    pagination.count = count;

    // pagination logic
    if (endIndex < count) {
      pagination.next = {
        page: page + 1,
      };
    } else {
      pagination.next = {
        page: null,
      };
    }
    if (startIndex > 0) {
      pagination.previous = {
        page: page - 1,
      };
    } else {
      pagination.previous = {
        page: null,
      };
    }
    // getting paginated products
    const doc = await Product.find({})
      .select("name image price")
      .sort({ createdAt: "asc" })
      .limit(limit)
      .skip(startIndex);
    res.send({ message: "success", doc: doc, metadata: pagination });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// product listing
const ProductOne = async (req, res) => {
  try {
    const { id } = req.params;
    const product = await Product.findOne({ _id: id }).select(
      "name description image price sku quantity color size stock"
    );
    // if (!product) throw NotFound("product not found");
    res.send({ message: "success", doc: product });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// products searching
const ProductSearch = async (req, res) => {
  try {
    // validation
    const data = await SearchValidation.validateAsync(req.body);
    const { word } = data;
    //  search a product
    const products = await Product.find({
      $or: [
        { name: new RegExp(word, "gi") },
        { description: new RegExp(word, "gi") },
      ],
    }).select("name  price image description category stock");
    // on success
    res.send({ doc: products });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// products filtering

// product update

// product delete
const ProductDelete = async (req, res) => {
  try {
    const { id } = req.params;
    const product = await Product.findOne({ _id: id });
    if (!product) throw NotFound("product not found");
    // update category population
    if (product.category != "category-x") {
      await Category.updateOne(
        { _id: product.category },
        { $pull: { products: product._id } }
      );
    }

    // update brand population
    if (product.brand != "brand-x") {
      await Brand.updateOne(
        { _id: product.brand },
        { $pull: { products: product._id } }
      );
    }

    // delete product
    await Product.deleteOne({ _id: id });
    res.send({ message: "success" });
  } catch (error) {
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// products delete many
module.exports = {
  ProductCreation,
  ProductsAll,
  ProductOne,
  ProductDelete,
  ProductSearch,
};
