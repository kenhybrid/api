// imports
import {
  CreationValidation,
  UpdateValidation,
} from "../validations/brandValidation.js";
import Brand from "../models/Brand.js";
// brand creation
const BrandCreation = async (req, res) => {
  try {
    const data = await CreationValidation.validateAsync(req.body);
    const { name } = data;
    // product creation
    const brand = await new Brand({
      name: name,
    });
    const doc = await brand.save();
    res.send({ message: "success", doc: doc });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// brand and products population listing

// brand update

// brand delete

// categories delete many
module.exports = {
  BrandCreation,
};
