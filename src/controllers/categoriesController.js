// imports
import {
  CreationValidation,
  UpdateValidation,
} from "../validations/categoryValidation.js";
import Category from "../models/Category.js";
// category creation
const CategoryCreation = async (req, res) => {
  try {
    const data = await CreationValidation.validateAsync(req.body);
    const { name } = data;
    // product creation
    const category = await new Category({
      name: name,
    });
    const doc = await category.save();
    res.send({ message: "success", doc: doc });
  } catch (error) {
    if (error.isJoi === true) error.status = 400;
    const error = {
      status: error.status,
      message: error.message,
    };
    res.send({ error: error });
  }
};
// category and products population listing

// category update

// category delete

// categories delete many
module.exports = {
  CategoryCreation,
};
