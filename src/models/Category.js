import mongoose from "mongoose";
import Product from "./Product.js";
//creating a categoriess schema
const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: Product }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("categories", categorySchema);
