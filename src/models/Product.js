// imports
import mongoose from "mongoose";
// schema
const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    brand: {
      type: String,
      default: "brand-x",
    },
    category: {
      type: String,
      default: "category-x",
    },
    color: [{ type: String }],
    size: [{ type: String }],
    sku: {
      type: String,
      uppercase: true,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    stock: {
      type: Number,
      default: 1,
    },
    image: {
      type: String,
      default:
        "https://assets.uigarage.net/content/uploads/2016/05/tumblr_o7rfj2J4qx1ul8y65o1_1280.jpg",
    },
    image_id: {
      type: String,
    },
  },
  { timestamps: true }
);
// product search
productSchema.index(
  { name: "text", description: "text" },
  { weights: { name: 10, description: 5 } }
);

module.exports = mongoose.model("products", productSchema);
