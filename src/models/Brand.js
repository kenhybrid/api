import mongoose from "mongoose";
import Product from "./Product.js";
// brands schema
const brandSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      default:
        "https://assets.uigarage.net/content/uploads/2016/05/tumblr_o7rfj2J4qx1ul8y65o1_1280.jpg",
    },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: Product }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("brands", brandSchema);
