import {
  ProductCreation,
  ProductsAll,
  ProductOne,
  ProductDelete,
  ProductSearch,
} from "../controllers/productsController.js";

const routes = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: ProductCreation,
  });
  app.route({
    method: "POST",
    url: "/search",
    handler: ProductSearch,
  });
  app.route({
    method: "GET",
    url: "/all",
    handler: ProductsAll,
  });
  app.route({
    method: "GET",
    url: "/one/:id",
    handler: ProductOne,
  });
  app.route({
    method: "DELETE",
    url: "/delete/:id",
    handler: ProductDelete,
  });
  done();
};

module.exports = routes;
