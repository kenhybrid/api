import { CategoryCreation } from "../controllers/categoriesController.js";

const routes = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: CategoryCreation,
  });

  done();
};

module.exports = routes;
