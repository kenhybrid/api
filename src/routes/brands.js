import { BrandCreation } from "../controllers/brandsController.js";

const routes = async (app, opts, done) => {
  app.route({
    method: "POST",
    url: "/create",
    handler: BrandCreation,
  });

  done();
};

module.exports = routes;
