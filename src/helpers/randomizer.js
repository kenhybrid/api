import crypto from "crypto";

const GenerateSKU = async () => {
  return "PRD-" + (await crypto.randomBytes(3).toString("hex"));
};
module.exports = {
  GenerateSKU,
};
