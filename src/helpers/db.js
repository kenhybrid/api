import mongoose from "mongoose";
import "dotenv/config.js";

export default async (fastify, options, done) => {
  try {
    await mongoose.connect(process.env.MONGODB_URI, {
      //   useNewUrlParser: true,
      //   useUnifiedTopology: true,
      //   useFindAndModify: false,
      //   useCreateIndex: true,
    });
    fastify.log.info("Database connection has been established");
    done();
  } catch (error) {
    fastify.log.error("Database Error:" + error);
    done(error);
  }
};
