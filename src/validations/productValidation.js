// imports
import Joi from "@hapi/joi";

// product creation
const CreationValidation = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().required(),
  brand: Joi.string(),
  category: Joi.string(),
  color: Joi.array().items(Joi.string()),
  size: Joi.array().items(Joi.string()),
  price: Joi.number().required(),
  stock: Joi.number(),
});
// product update

// products search
const SearchValidation = Joi.object({
  word: Joi.string()
    .lowercase()
    .trim()
    .regex(/[$\(\)<>]/, { invert: true }),
});
// products filtering

module.exports = {
  CreationValidation,
  SearchValidation,
};
