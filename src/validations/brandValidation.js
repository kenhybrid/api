// imports
import Joi from "@hapi/joi";

const CreationValidation = Joi.object({
  name: Joi.string().required(),
});

const UpdateValidation = Joi.object({
  name: Joi.string(),
});

module.exports = {
  CreationValidation,
  UpdateValidation,
};
